<%--
  Created by IntelliJ IDEA.
  User: Drago
  Date: 3/30/2021
  Time: 2:13 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Movie List</title>
</head>
<header>
    <nav class="navbar navbar-expand-md navbar-dark"
         style="background-color: blue">
        <ul class="navbar-nav">
            <li><a href="movies/movies-form/list"
                   class="nav-link">Movies</a></li>
        </ul>

        <ul class="navbar-nav">
            <li><a href="friends/friends-form/list"
                   class="nav-link">Friends</a></li>
        </ul>

        <ul class="navbar-nav navbar-collapse justify-content-end">
            <li><a href="<%=request.getContextPath()%>/logout"
                   class="nav-link">Logout</a></li>
        </ul>
    </nav>
</header>

<div class="row">
    <!-- <div class="alert alert-success" *ngIf='message'>{{message}}</div> -->

    <div class="container">
        <h3 class="text-center">List of Movies</h3>
        <hr>
        <div class="container text-left">

            <a href="<%=request.getContextPath()%>/new"
               class="btn btn-success">Add Movie</a>
        </div>
        <br>
        <table class="table table-bordered">
            <thead>
            <tr>
                <th>Movie Title</th>
                <th>Username</th>
                <th>Actions</th>
            </tr>
            </thead>
            <tbody>
            <!--   for (Movies movies: movies) {  -->
            <c:forEach var="movies" items="${listMovies}">

                <tr>
                    <td><c:out value="${movies.movieTitles}" /></td>
                    <td><c:out value="${movies.username}" /></td>

                    <td><a href="edit?id=<c:out value='${movies.id}' />">Edit</a>
                        &nbsp;&nbsp;&nbsp;&nbsp; <a
                                href="delete?id=<c:out value='${movies.id}' />">Delete</a></td>

                    <!--  <td><button (click)="updateTodo(movies.id)" class="btn btn-success">Update</button>
                              <button (click)="deleteTodo(movies.id)" class="btn btn-warning">Delete</button></td> -->
                </tr>
            </c:forEach>
            <!-- } -->
            </tbody>

        </table>
    </div>
</div>

<jsp:include page="../common/footer.jsp"></jsp:include>
</body>
</html>
