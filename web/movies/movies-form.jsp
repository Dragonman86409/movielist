<%--
  Created by IntelliJ IDEA.
  User: Drago
  Date: 3/30/2021
  Time: 8:24 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Movies Form</title>
</head>
<body>
<header>
    <nav class="navbar navbar-expand-md navbar-dark"
         style="background-color: blue">
        <ul class="navbar-nav">
            <li><a href="movies/movies-form/list"
                   class="nav-link">Movies</a></li>
        </ul>

        <ul class="navbar-nav">
            <li><a href="friends/friends-form/list"
                   class="nav-link">Friends</a></li>
        </ul>

        <ul class="navbar-nav navbar-collapse justify-content-end">
            <li><a href="<%=request.getContextPath()%>/logout"
                   class="nav-link">Logout</a></li>
        </ul>
    </nav>
</header>
<div class="container col-md-5">
    <div class="card">
        <div class="card-body">
            <c:if test="${movies != null}">
            <form action="update" method="post">
                </c:if>
                <c:if test="${movies == null}">
                <form action="insert" method="post">
                    </c:if>

                    <caption>
                        <h2>
                            <c:if test="${movies != null}">
                                Edit Movies
                            </c:if>
                            <c:if test="${movies == null}">
                                Add New Movie
                            </c:if>
                        </h2>
                    </caption>

                    <c:if test="${movies != null}">
                        <input type="hidden" name="idmovies" value="<c:out value='${movies.idmovies}' />" />
                    </c:if>

                    <fieldset class="form-group">
                        <label>Movie Title</label> <input type="text"
                                                         value="<c:out value='${movies.movieTitle}' />" class="form-control"
                                                         name="movieTitles" required="required" minlength="1">
                    </fieldset>

                    <fieldset class="form-group">
                        <label>Todo Decription</label> <input type="text"
                                                              value="<c:out value='${movies.description}' />" class="form-control"
                                                              name="description" minlength="5">
                    </fieldset>

                    <fieldset class="form-group">
                        <label>Todo Status</label> <select class="form-control"
                                                           name="isDone">
                        <option value="false">In Progress</option>
                        <option value="true">Complete</option>
                    </select>
                    </fieldset>

                    <fieldset class="form-group">
                        <label>Todo Target Date</label> <input type="date"
                                                               value="<c:out value='${movies.targetDate}' />" class="form-control"
                                                               name="targetDate" required="required">
                    </fieldset>

                    <button type="submit" class="btn btn-success">Save</button>
                </form>
        </div>
    </div>
</div>

<jsp:include page="../common/footer.jsp"></jsp:include>
</body>
</html>
