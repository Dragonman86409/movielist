<%--
  Created by IntelliJ IDEA.
  User: Joel Anderson
  Date: 3/27/2021
  Time: 12:15 PM
--%>
<%@ page contentType="text/html; charset=UTF-8" language="java" %>
<html>
<head>
    <title>Login</title>
    <style>
        body {
            background-color: white;
        }
    </style>
</head>
<body>
<jsp:include page="../common/header.jsp"></jsp:include>
<div class="container col-md-8 col-md-offset-3" style="overflow: auto">
    <h1>Login Form</h1>
    <form action="<%=request.getContextPath()%>/login" method="post">

        <div class="form-group">
            <label for="uname">User Name:</label> <input type="text" class="form-control" id="username"
                                                         placeholder="User Name" name="username" required>
        </div>

        <div class="form-group">
            <label for="uname">Password:</label> <input type="password" class="form-control" id="password"
                                                        placeholder="Password" name="password" required>
        </div>


        <button type="submit" class="btn btn-primary">Submit</button>
    </form>
</div>
<jsp:include page="../common/footer.jsp"></jsp:include>
</body>
</html>
