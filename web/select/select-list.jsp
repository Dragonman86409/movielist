<%--
  Created by IntelliJ IDEA.
  User: Drago
  Date: 3/30/2021
  Time: 2:40 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
</head>

<body>
<jsp:include page="../common/header.jsp"></jsp:include>
<nav class="navbar navbar-expand-md navbar-dark"
     style="background-color: blue">
    <ul class="navbar-nav">
        <li><a href="movies/movies-form/list"
               class="nav-link">Movies</a></li>
    </ul>

    <ul class="navbar-nav">
        <li><a href="friends/friends-form/list"
               class="nav-link">Friends</a></li>
    </ul>

    <ul class="navbar-nav navbar-collapse justify-content-end">
        <li><a href="<%=request.getContextPath()%>/logout"
               class="nav-link">Logout</a></li>
    </ul>
</nav>
<jsp:include page="../common/footer.jsp"></jsp:include>
</body>
</html>
