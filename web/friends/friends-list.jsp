<%--
  Created by IntelliJ IDEA.
  User: Drago
  Date: 3/30/2021
  Time: 2:13 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Friends List</title>
</head>
<header>
    <nav class="navbar navbar-expand-md navbar-dark"
         style="background-color: blue">
        ul class="navbar-nav">
        <li><a href="movies/movies-form/list"
               class="nav-link">Movies</a></li>
        </ul>

        <ul class="navbar-nav">
            <li><a href="friends/friends-form/list"
                   class="nav-link">Friends</a></li>
        </ul>

        <ul class="navbar-nav navbar-collapse justify-content-end">
            <li><a href="<%=request.getContextPath()%>/logout"
                   class="nav-link">Logout</a></li>
        </ul>
    </nav>
</header>

<div class="row">
    <!-- <div class="alert alert-success" *ngIf='message'>{{message}}</div> -->

    <div class="container">
        <h3 class="text-center">List of Todos</h3>
        <hr>
        <div class="container text-left">

            <a href="<%=request.getContextPath()%>/new"
               class="btn btn-success">Add Todo</a>
        </div>
        <br>
        <table class="table table-bordered">
            <thead>
            <tr>
                <th>Name</th>
                <th>Borrow Date</th>
                <th>Movie 1</th>
                <th>Movie 2</th>
                <th>Movie 3</th>
                <th>Actions</th>
            </tr>
            </thead>
            <tbody>
            <!--   for (Friends friends: friends) {  -->
            <c:forEach var="friends" items="${listFriends}">

                <tr>
                    <td><c:out value="${friends.title}" /></td>
                    <td><c:out value="${friends.borrow_ate}" /></td>
                    <td><c:out value="${friends.movie1}" /></td>
                    <td><c:out value="${friends.movie2}" /></td>
                    <td><c:out value="${friends.movie3}" /></td>

                    <td><a href="edit?id=<c:out value='${friends.id}' />">Edit</a>
                        &nbsp;&nbsp;&nbsp;&nbsp; <a
                                href="delete?id=<c:out value='${friends.id}' />">Delete</a></td>

                    <!--  <td><button (click)="updateTodo(friends.id)" class="btn btn-success">Update</button>
                              <button (click)="deleteTodo(friends.id)" class="btn btn-warning">Delete</button></td> -->
                </tr>
            </c:forEach>
            <!-- } -->
            </tbody>

        </table>
    </div>
</div>

<jsp:include page="../common/footer.jsp"></jsp:include>
</body>
</html>
