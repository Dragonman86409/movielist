<%--
  Created by IntelliJ IDEA.
  User: Drago
  Date: 3/30/2021
  Time: 8:24 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
</head>
<body>
<header>
    <nav class="navbar navbar-expand-md navbar-dark"
         style="background-color: tomato">
        <ul class="navbar-nav">
            <li><a href="<%=request.getContextPath()%>/list"
                   class="nav-link">Movies</a></li>
        </ul>

        <ul class="navbar-nav">
            <li><a href="<%=request.getContextPath()%>/list"
                   class="nav-link">Friends</a></li>
        </ul>

        <ul class="navbar-nav navbar-collapse justify-content-end">
            <li><a href="<%=request.getContextPath()%>/logout"
                   class="nav-link">Logout</a></li>
        </ul>
    </nav>
</header>
<div class="container col-md-5">
    <div class="card">
        <div class="card-body">
            <c:if test="${friends != null}">
            <form action="update" method="post">
                </c:if>
                <c:if test="${friends == null}">
                <form action="insert" method="post">
                    </c:if>

                    <caption>
                        <h2>
                            <c:if test="${friends != null}">
                                Edit Movies
                            </c:if>
                            <c:if test="${friends == null}">
                                Add New Movie
                            </c:if>
                        </h2>
                    </caption>

                    <c:if test="${friends != null}">
                        <input type="hidden" name="idfriends" value="<c:out value='${friends.idfriends}' />" />
                    </c:if>

                    <fieldset class="form-group">
                        <label>Friends Name</label> <input type="text"
                                                          value="<c:out value='${friends.name}' />" class="form-control"
                                                          name="name" required="required" minlength="1">
                    </fieldset>

                        <fieldset class="form-group">
                            <label>Date of borrowed movie</label> <input type="date"
                                                                   value="<c:out value='${friends.borrow_date}' />" class="form-control"
                                                                   name="borrow_date" required="required">
                        </fieldset>

                        <fieldset class="form-group">
                            <label>Movie 1</label> <input type="text"
                                                          value="<c:out value='${friends.movie1}' />" class="form-control"
                                                          name="movie1" minlength="1">
                        </fieldset>
                        <fieldset class="form-group">
                            <label>Movie 2</label> <input type="text"
                                                          value="<c:out value='${friends.movie1}' />" class="form-control"
                                                          name="movie2" minlength="1">
                        </fieldset>
                        <fieldset class="form-group">
                            <label>Movie 3</label> <input type="text"
                                                          value="<c:out value='${friends.movie1}' />" class="form-control"
                                                          name="movie3" minlength="1">
                        </fieldset>


                    <button type="submit" class="btn btn-success">Save</button>
                </form>
        </div>
    </div>
</div>

<jsp:include page="../common/footer.jsp"></jsp:include>
</body>
</html>
