<%--
  Created by IntelliJ IDEA.
  User: Joel Anderson
  Date: 3/27/2021
  Time: 11:55 AM
--%>
<%@ page contentType="text/html; charset=UTF-8" language="java" %>
<html>
<head>
    <title>Register</title>
    <style>
        body {
            background-color: white;
        }
    </style>
</head>
<body>
<jsp:include page="../common/header.jsp"></jsp:include>
<div class="container">

    <h2>User Register Form</h2>
    <div class="col-md-6 col-md-offset-3">
        <div class="alert alert-success center" role="alert">
            <p>${NOTIFICATION}</p>
        </div>

        <form action="<%=request.getContextPath()%>/register" method="post">

            <div class="form-group">
                <label for="uname">First Name:</label> <input type="text" class="form-control" id="uname1" placeholder="First Name" name="firstName" required>
            </div>

            <div class="form-group">
                <label for="uname">Last Name:</label> <input type="text" class="form-control" id="uname2" placeholder="last Name" name="lastName" required>
            </div>

            <div class="form-group">
                <label for="uname">User Name:</label> <input type="text" class="form-control" id="username" placeholder="User Name" name="username" required>
            </div>

            <div class="form-group">
                <label for="uname">Password:</label> <input type="password" class="form-control" id="password" placeholder="Password" name="password" required>
            </div>

            <button type="submit" class="btn btn-primary">Submit</button>

        </form>
    </div>
</div>
<jsp:include page="../common/footer.jsp"></jsp:include>
</body>
</html>
