<%--
  Created by IntelliJ IDEA.
  User: Joel Anderson
  Date: 3/27/2021
  Time: 11:58 AM
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<header>
    <nav class="navbar navbar-expand-md navbar-dark"
         style="background-color: blue">
        <ul class="navbar-nav navbar-collapse justify-content-end">
            <li><a href="<%= request.getContextPath() %>/login" class="nav-link">Login</a></li>
            <li><a href="<%= request.getContextPath() %>/register" class="nav-link">Signup</a></li>
        </ul>
    </nav>
</header>
