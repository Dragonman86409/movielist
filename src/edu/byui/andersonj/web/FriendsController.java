package edu.byui.andersonj.web;

import java.io.IOException;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import edu.byui.andersonj.dao.FriendsDao;
import edu.byui.andersonj.dao.FriendsDaoImpl;
import edu.byui.andersonj.model.Friends;

public class FriendsController extends HttpServlet {

    private static final long serialVersionUID = 1;
    private FriendsDao friendsDao;

    public void init() {
        friendsDao = (FriendsDao) new FriendsDaoImpl();
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        doGet(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String action = request.getServletPath();

        try {
            switch (action) {
                case "/new":
                    showNewForm(request, response);
                    break;
                case "/insert":
                    insertFriends(request, response);
                    break;
                case "/delete":
                    deleteFriends(request, response);
                    break;
                case "/edit":
                    showEditForm(request, response);
                    break;
                case "/update":
                    updateFriends(request, response);
                    break;
                case "/list":
                    listFriends(request, response);
                    break;
                default:
                    RequestDispatcher dispatcher = request.getRequestDispatcher("login/login.jsp");
                    dispatcher.forward(request, response);
                    break;
            }
        } catch (SQLException ex) {
            throw new ServletException(ex);
        }
    }

    private void listFriends(HttpServletRequest request, HttpServletResponse response)
            throws SQLException, IOException, ServletException {
        List<Friends> ListFriends = FriendsDao.selectAllFriends();
        request.setAttribute("ListFriends", ListFriends);
        RequestDispatcher dispatcher = request.getRequestDispatcher("friends/friends-list.jsp");
        dispatcher.forward(request, response);
    }

    private void showNewForm(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        RequestDispatcher dispatcher = request.getRequestDispatcher("friends/friends-form.jsp");
        dispatcher.forward(request, response);
    }

    private void showEditForm(HttpServletRequest request, HttpServletResponse response)
            throws SQLException, ServletException, IOException {
        int id = Integer.parseInt(request.getParameter("id"));
        FriendsDao existingFriends = (FriendsDao) FriendsDao.selectFriends(id);
        RequestDispatcher dispatcher = request.getRequestDispatcher("friends/friends-form.jsp");
        request.setAttribute("friends", existingFriends);
        dispatcher.forward(request, response);

    }

    private void insertFriends(HttpServletRequest request, HttpServletResponse response) throws SQLException, IOException {

        String name = request.getParameter("title");
        LocalDate borrow_date = LocalDate.parse(request.getParameter("borrow_date"));
        Integer movie1= Integer.valueOf(request.getParameter("movie1"));
        Integer movie2= Integer.valueOf(request.getParameter("movie2"));
        Integer movie3= Integer.valueOf(request.getParameter("movie3"));

        Friends newFriends = new Friends(name, borrow_date, movie1, movie2, movie3);
        friendsDao.insertFriends(newFriends);
        response.sendRedirect("list");
    }

    private void updateFriends(HttpServletRequest request, HttpServletResponse response) throws SQLException, IOException {
        int id = Integer.parseInt(request.getParameter("id"));

        String name = request.getParameter("title");
        LocalDate borrow_date = LocalDate.parse(request.getParameter("borrow_date"));
        Integer movie1= Integer.valueOf(request.getParameter("movie1"));
        Integer movie2= Integer.valueOf(request.getParameter("movie2"));
        Integer movie3= Integer.valueOf(request.getParameter("movie3"));

        Friends updateFriends = new Friends(name, borrow_date, movie1, movie2, movie3);

        FriendsDao.updateFriends(updateFriends);

        response.sendRedirect("list");
    }

    private void deleteFriends(HttpServletRequest request, HttpServletResponse response) throws SQLException, IOException {
        int id = Integer.parseInt(request.getParameter("id"));
        friendsDao.deleteFriends(id);
        response.sendRedirect("list");
    }

}
