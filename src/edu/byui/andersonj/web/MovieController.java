package edu.byui.andersonj.web;

import java.io.IOException;
import java.sql.SQLException;
import java.util.Queue;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import edu.byui.andersonj.dao.MoviesDao;
import edu.byui.andersonj.dao.MoviesDaoImpl;
import edu.byui.andersonj.model.Movies;

public class MovieController extends HttpServlet{

    private static final long serialVersionUID = 1;
    private MoviesDao moviesDao;

    public void init() {
        moviesDao = (MoviesDao) new MoviesDaoImpl();
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        doGet(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String action = request.getServletPath();

        try {
            switch (action) {
                case "/new":
                    showNewForm(request, response);
                    break;
                case "/insert":
                    insertMovies(request, response);
                    break;
                case "/delete":
                    deleteMovies(request, response);
                    break;
                case "/edit":
                    showEditForm(request, response);
                    break;
                case "/update":
                    updateMovies(request, response);
                    break;
                case "/list":
                    listMovies(request, response);
                    break;
                default:
                    RequestDispatcher dispatcher = request.getRequestDispatcher("login/login.jsp");
                    dispatcher.forward(request, response);
                    break;
            }
        } catch (SQLException ex) {
            throw new ServletException(ex);
        }
    }

    private void listMovies(HttpServletRequest request, HttpServletResponse response)
            throws SQLException, IOException, ServletException {
        Queue<Movies> queueMovies = MoviesDao.selectAllMovies();
        request.setAttribute("queueMovies", queueMovies);
        RequestDispatcher dispatcher = request.getRequestDispatcher("movies/movies-list.jsp");
        dispatcher.forward(request, response);
    }

    private void showNewForm(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        RequestDispatcher dispatcher = request.getRequestDispatcher("movies/movies-form.jsp");
        dispatcher.forward(request, response);
    }

    private void showEditForm(HttpServletRequest request, HttpServletResponse response)
            throws SQLException, ServletException, IOException {
        int id = Integer.parseInt(request.getParameter("id"));
        MoviesDao existingMovies = (MoviesDao) MoviesDao.selectMovies(id);
        RequestDispatcher dispatcher = request.getRequestDispatcher("movies/movies-form.jsp");
        request.setAttribute("movies", existingMovies);
        dispatcher.forward(request, response);

    }

    private void insertMovies(HttpServletRequest request, HttpServletResponse response) throws SQLException, IOException {

        String movieTitles = request.getParameter("title");
        String username = request.getParameter("username");

        Movies newMovies = new Movies(movieTitles, username);
        moviesDao.insertMovies(newMovies);
        response.sendRedirect("list");
    }

    private void updateMovies(HttpServletRequest request, HttpServletResponse response) throws SQLException, IOException {
        int id = Integer.parseInt(request.getParameter("id"));

        String title = request.getParameter("title");
        String username = request.getParameter("username");

        Movies updateMovies = new Movies(id, title, username);

        MoviesDao.updateMovies(updateMovies);

        response.sendRedirect("list");
    }

    private void deleteMovies(HttpServletRequest request, HttpServletResponse response) throws SQLException, IOException {
        int id = Integer.parseInt(request.getParameter("id"));
        moviesDao.deleteMovies(id);
        response.sendRedirect("list");
    }

}
