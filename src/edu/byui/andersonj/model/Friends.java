package edu.byui.andersonj.model;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Table(name = "friends")

public class Friends {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idfriends")
    private int idfriends;

    @Column(name = "name")
    private String name;

    @Column(name = "borrow_date")
    private LocalDate borrow_date;

    @Column(name = "movie1")
    private int movie1;

    @Column(name = "movie2")
    private int movie2;

    @Column(name = "movie3")
    private int movie3;

    public int getIdfriends() {return idfriends;}

    public void setIdfriends(int idfriends) {this.idfriends = idfriends;}

    public String getName() {return name;}

    public void setName(String name) {this.name = name;}

    public LocalDate getBorrow_date() {return borrow_date;}

    public void setBorrow_date(LocalDate borrow_date) {this.borrow_date = borrow_date;}

    public int getMovie1() {return movie1;}

    public void setMovie1(int movie1) {this.movie1 = movie1;}

    public int getMovie2() {return movie2;}

    public void setMovie2(int movie2) {this.movie2 = movie2;}

    public int getMovie3() {return movie3;}

    public void setMovie3(int movie3) {this.movie3 = movie3;}

}
