package edu.byui.andersonj.model;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "movies")

public class Movies implements Serializable {

    /*
     * id is an identity type field in the database and the primary key
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idmovie")
    private int idmovies;

    @Column(name = "movieTitles")
    private String movieTitles;

    @Column(name = "username")
    private String username;


    public int getIdmovies() {
        return idmovies;
    }

    public void setIdmovies (int idmovies) {
        this.idmovies = idmovies;
    }

    public String getMovieTitles() {
        return movieTitles;
    }

    public void setMovieTitles(String movieTitles) {
        this.movieTitles = movieTitles;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

}
