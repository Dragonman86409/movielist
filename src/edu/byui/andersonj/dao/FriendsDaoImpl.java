package edu.byui.andersonj.dao;

import edu.byui.andersonj.model.Friends;
import edu.byui.andersonj.utils.HibernateUtils;

import java.sql.*;
import java.util.*;
import java.time.LocalDate;

public class FriendsDaoImpl {

    private static final String INSERT_FRIENDS_SQL = "INSERT INTO friends" +
            "  (name, description, borrow_date,  is_done) VALUES " + " (?, ?, ?, ?, ?);";

    private static final String SELECT_FRIENDS_BY_ID = "select name,borrow_date,movie1,movie2,movie3 from friends where idfriends =?";
    private static final String SELECT_ALL_FRIENDS = "select * from friends";
    private static final String DELETE_FRIENDS_BY_ID = "delete from friends where idfriends = ?;";
    private static final String UPDATE_FRIENDS = "update friends set name= ?, borrow_date =?, movie1 = ?, movie2 = ?, movie3 = ? where idfriends = ?;";

    public FriendsDaoImpl() {
    }

    @Override
    public void insertFriends(Friends todo) throws SQLException {
        System.out.println(INSERT_FRIENDS_SQL);
        // try-with-resource statement will auto close the connection.
        try (Connection connection = HibernateUtils.getConnection(); PreparedStatement preparedStatement = connection.prepareStatement(INSERT_FRIENDS_SQL)) {
            preparedStatement.setString(1, todo.getName());
            preparedStatement.setDate(2, HibernateUtils.getSQLDate(todo.getBorrow_date()));
            preparedStatement.setInt(3, todo.getMovie1());
            preparedStatement.setInt(4, todo.getMovie2());
            preparedStatement.setInt(5, todo.getMovie3());
            System.out.println(preparedStatement);
            preparedStatement.executeUpdate();
        } catch (SQLException exception) {
            HibernateUtils.printSQLException(exception);
        }
    }

    @Override
    public Friends selectFriends(int idfriends) {
        Friends friends = null;
        // Step 1: Establishing a Connection
        try (Connection connection = HibernateUtils.getConnection();
             // Step 2:Create a statement using connection object
             PreparedStatement preparedStatement = connection.prepareStatement(SELECT_FRIENDS_BY_ID);) {
            preparedStatement.setInt(1, idfriends);
            System.out.println(preparedStatement);
            // Step 3: Execute the query or update query
            ResultSet rs = preparedStatement.executeQuery();

            // Step 4: Process the ResultSet object.
            while (rs.next()) {
                Integer idfriends = rs.getInt("idfriends");
                String name = rs.getString("name");
                LocalDate borrow_date = rs.getDate("borrow_date").toLocalDate();
                Integer movie1 = rs.getInt("movie1");
                Integer movie2 = rs.getInt("movie2");
                Integer movie3 = rs.getInt("movie3");
                friends = new Friends(idfriends, name, borrow_date, movie1, movie2, movie3);
            }
        } catch (SQLException exception) {
            HibernateUtils.printSQLException(exception);
        }
        return friends;
    }

    @Override
    public List<Friends> selectAllFriends() {

        // using try-with-resources to avoid closing resources (boiler plate code)
        List<Friends> friends = new ArrayList<>();

        // Step 1: Establishing a Connection
        try (Connection connection = HibernateUtils.getConnection();

             // Step 2:Create a statement using connection object
             PreparedStatement preparedStatement = connection.prepareStatement(SELECT_ALL_FRIENDS);) {
            System.out.println(preparedStatement);
            // Step 3: Execute the query or update query
            ResultSet rs = preparedStatement.executeQuery();

            // Step 4: Process the ResultSet object.
            while (rs.next()) {

                Integer idfriends = rs.getInt("idfriends");
                String name = rs.getString("name");
                LocalDate borrow_date = rs.getDate("borrow_date").toLocalDate();
                Integer movie1 = rs.getInt("movie1");
                Integer movie2 = rs.getInt("movie2");
                Integer movie3 = rs.getInt("movie3");
                friends.add(new Friends(idfriends, name, borrow_date, movie1, movie2, movie3));
            }
        } catch (SQLException exception) {
            HibernateUtils.printSQLException(exception);
        }
        return friends;
    }

    @Override
    public boolean deleteFriends(int idfriends) throws SQLException {
        boolean rowDeleted;
        try (Connection connection = HibernateUtils.getConnection(); PreparedStatement statement = connection.prepareStatement(DELETE_FRIENDS_BY_ID);) {
            statement.setInt(1, idfriends);
            rowDeleted = statement.executeUpdate() > 0;
        }
        return rowDeleted;
    }

    @Override
    public boolean updateFriends(Friends todo) throws SQLException {
        boolean rowUpdated;
        try (Connection connection = HibernateUtils.getConnection(); PreparedStatement statement = connection.prepareStatement(UPDATE_FRIENDS);) {
            statement.setString(1, todo.getName());
            statement.setDate(2, HibernateUtils.getSQLDate(todo.getBorrow_date()));
            statement.setInt(3, todo.getMovie1());
            statement.setInt(4, todo.getMovie2());
            statement.setInt(5, todo.getMovie3());
            rowUpdated = statement.executeUpdate() > 0;
        }
        return rowUpdated;

    }
}
