package edu.byui.andersonj.dao;

import edu.byui.andersonj.model.Friends;

import java.sql.SQLException;
import java.util.List;

public interface FriendsDao {

    void insertFriends(Friends friends) throws SQLException;

    static Friends selectFriends(int idfriends);

    static List<Friends> selectAllFriends() {
        return null;
    }

    void deleteFriends(int id) throws SQLException;

    static boolean updateFriends(Friends friends) throws SQLException;

}
