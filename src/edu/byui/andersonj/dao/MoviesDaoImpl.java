package edu.byui.andersonj.dao;

import edu.byui.andersonj.model.Movies;
import edu.byui.andersonj.utils.HibernateUtils;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.PriorityQueue;
import java.util.Queue;

public class MoviesDaoImpl {

    private static final String INSERT_MOVIES_SQL = "INSERT INTO movies" +
            "  (movieTitles, username) VALUES " + " (?, ?);";

    private static final String SELECT_MOVIES_BY_ID = "select movieTitles, username from movies where id =?";
    private static final String SELECT_ALL_MOVIES = "select * from movies";
    private static final String DELETE_MOVIES_BY_ID = "delete from movies where id = ?;";
    private static final String UPDATE_MOVIES = "update movies set movieTitles = ?, username= ?;";

    public MoviesDaoImpl() {}

    @Override
    public void insertMovies(Movies movies) throws SQLException {
        System.out.println(INSERT_MOVIES_SQL);
        // try-with-resource statement will auto close the connection.
        try (Connection connection = HibernateUtils.getConnection(); PreparedStatement preparedStatement = connection.prepareStatement(INSERT_MOVIES_SQL)) {
            preparedStatement.setString(1, movies.getMovieTitles());
            preparedStatement.setString(2, movies.getUsername());
            System.out.println(preparedStatement);
            preparedStatement.executeUpdate();
        } catch (SQLException exception) {
            HibernateUtils.printSQLException(exception);
        }
    }

    @Override
    public Movies selectMovies(int moviesId) {
        Movies movies = null;
        // Step 1: Establishing a Connection
        try (Connection connection = HibernateUtils.getConnection();
             // Step 2:Create a statement using connection object
             PreparedStatement preparedStatement = connection.prepareStatement(SELECT_MOVIES_BY_ID);) {
            preparedStatement.setInt(1, moviesId);
            System.out.println(preparedStatement);
            // Step 3: Execute the query or update query
            ResultSet rs = preparedStatement.executeQuery();

            // Step 4: Process the ResultSet object.
            while (rs.next()) {
                int idmovies = rs.getInt("id");
                String movietitles = rs.getString("Movie Title");
                String username = rs.getString("Owner");
                movies = new Movies(idmovies, movietitles, username);
            }
        } catch (SQLException exception) {
            HibernateUtils.printSQLException(exception);
        }
        return movies;
    }

    @Override
    public Queue<Movies> selectAllMovies() {

        // using try-with-resources to avoid closing resources (boiler plate code)
        Queue<Movies> movies = new PriorityQueue<>();

        // Step 1: Establishing a Connection
        try (Connection connection = HibernateUtils.getConnection();

             // Step 2:Create a statement using connection object
             PreparedStatement preparedStatement = connection.prepareStatement(SELECT_ALL_MOVIES);) {
            System.out.println(preparedStatement);
            // Step 3: Execute the query or update query
            ResultSet rs = preparedStatement.executeQuery();

            // Step 4: Process the ResultSet object.
            while (rs.next()) {
                String movieTitles = rs.getString("Movie title");
                movies.add(new Movies(movieTitles));
            }
        } catch (SQLException exception) {
            HibernateUtils.printSQLException(exception);
        }
        return movies;
    }

    @Override
    public boolean deleteMovies(int id) throws SQLException {
        boolean rowDeleted;
        try (Connection connection = HibernateUtils.getConnection(); PreparedStatement statement = connection.prepareStatement(DELETE_MOVIES_BY_ID);) {
            statement.setInt(1, id);
            rowDeleted = statement.executeUpdate() > 0;
        }
        return rowDeleted;
    }

    @Override
    public boolean updateMovies(Movies movies) throws SQLException {
        boolean rowUpdated;
        try (Connection connection = HibernateUtils.getConnection(); PreparedStatement statement = connection.prepareStatement(UPDATE_MOVIES);) {
            statement.setString(1, movies.getMovieTitles());
            statement.setString(2, movies.getUsername());
            statement.setInt(3, movies.getIdmovies());
            rowUpdated = statement.executeUpdate() > 0;
        }
        return rowUpdated;

    }
}