package edu.byui.andersonj.dao;

import edu.byui.andersonj.model.Movies;

import java.sql.SQLException;
import java.util.Queue;

public interface MoviesDao {

    static void updateMovies(Movies updateMovies) {
    }

    void insertMovies(Movies movies) throws SQLException;

    static Movies selectMovies(int moviesId);

    static Queue<Movies> selectAllMovies();

    boolean deleteMovies(int id) throws SQLException;

    boolean updateMovie(Movies movies) throws SQLException;

}
